----------------------
Projet : Table de multiplication
----------------------

Le projet consisté à concevoir une application web qui permet lorsque l'on rentre un chiffre ou numbre l'apllication nous donnes ,
sa table de multiplication , ensuite d'affiché en dessous les tables de multiplication de 1 à 10 , et finalement d'affiché selon un
nombres données plusieurs tables.

-------------------------
Installations nécessaire :
-------------------------

1. Angular

-----------------------
Présentation de l’équipe :
-----------------------

Ce projet a été développé entre le 09/11/2020 et le 12/11/2020 par Angeles Théo (Etudiant en deuxième année de BTS SIO SLAM).


Theo était chargé de développer l'application web avec ses deux composants.

---------
Le code :
---------

#### Voici l'index.html ( Localisation : src/index.html )


    <!DOCTYPE HTML>
        <html lang="fr">
            <head>
            <title>Les tables</title>
            <meta charset="utf-8" />
             <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
            </head>
                <body class="is-preload">
                    <app-root></app-root>
                </body>
        </html>


#### Voici le composant , app.component.html ( Localisation : src/app/app.component.html )

    <app-table-multiplication></app-table-multiplication>

    <div class="mt-5">
        <app-tables-multiplication></app-tables-multiplication>
    </div>

#### Voici le composant , table-multiplication.component.html ( Localisation : src/app/table-multiplication/table-multiplication.component.html )

    <section>
    <div class="container">
        <div class="column">
        <h1 class="title is-1">{{title}}</h1>
            </div>
        </div>
    </section>

    <div class="mt-5">
        <div class="container">
            <form [formGroup]="identForm" (ngSubmit)="ident()">
                <div class="field">
                    <div class="control">
                <label>
                    <input class="input is-success" formControlName="nombre" type="text"
                        placeholder="Veuillez entrer un chiffre ou un nombre">
                </label>
                </div>
             </div>
            <div class="field is-grouped">
                <div class="control">
                <button class="button is-link">Soumettre</button>
            </div>
        </div>

      <div class="mt-5">
        <ul *ngFor="let i of table">
          <li>{{ chiffre }}x{{ i }} = {{ chiffre * i }}
        </ul>
      </div>
    </form>
    </div>
    </div>

#### Voici le composant , table-multiplication.component.ts ( Localisation : src/app/table-multiplication/table-multiplication.component.ts )

    import { Component, OnInit } from '@angular/core';
    import { FormControl, FormGroup } from '@angular/forms';

    @Component({
        selector: 'app-table-multiplication',
        templateUrl: './table-multiplication.component.html',
        styleUrls: ['./table-multiplication.component.scss']
    })
    export class TableMultiplicationComponent implements OnInit {
        title = 'Le créateur de table :';
        table =  [1, 2, 3, 4, 5, 6, 7, 8, 9, 10] ;
        chiffre = 0 ;

    identForm!: FormGroup;

    constructor() { }

    ngOnInit(): void {
        this.identForm = new FormGroup({
        nombre:  new FormControl()
        });

    }
    // tslint:disable-next-line:typedef
    ident() {
        console.log('Données du formulaire...', this.identForm.value);
        this.chiffre = this.identForm.get('nombre')?.value ;
        }
    }

#### Voici le composant , tables-multiplication.component.html ( Localisation : src/app/tables-multiplication/tables-multiplication.component.html )

    <div class="container">
    <h1 class="title is-1" >Les tables de multiplications : </h1>
    <img src="assets/images/tables.png" alt="">
    </div>

    <div class="container">
    <h2 class="title is-1" >Générateur de table : </h2>
    </div>

    <div class="container">
        <div class="mt-5">
        <form [formGroup]="identForm" (ngSubmit)="ident()">
        <div class="field">
        <div class="control">
            <label>
            <input class="input is-success" formControlName="nombre" type="text" placeholder="Veuillez entrer le nombre de tables désiré">
        </label>
      </div>
    </div>
    <div class="field is-grouped">
      <div class="control">
        <button class="button is-link">Soumettre</button>
      </div>
    </div>

    <ul *ngFor="let lenombre  of demandes">
      <ul *ngFor="let i  of tables">
        <li>{{ lenombre }}x{{ i }} = {{ i * lenombre }}
      </ul>
    </ul>
    </form>
    </div>
    </div>

#### Voici le composant , tables-multiplication.component.ts ( Localisation : src/app/tables-multiplication/tables-multiplication.component.ts )

    import { Component, OnInit } from '@angular/core';
    import {FormControl, FormGroup} from '@angular/forms';

    @Component({
      selector: 'app-tables-multiplication',
      templateUrl: './tables-multiplication.component.html',
      styleUrls: ['./tables-multiplication.component.scss']
    })
    export class TablesMultiplicationComponent implements OnInit {
      identForm!: FormGroup;
      tables =  [1, 2, 3, 4, 5, 6, 7, 8, 9, 10] ;
      demandes = [55] ;


      constructor() { }

      ngOnInit(): void {
        this.identForm = new FormGroup({
          nombre:  new FormControl()
        });

      }

      // tslint:disable-next-line:typedef
      ident() {
        console.log('Données du formulaire...', this.identForm.value);
        this.demandes = this.identForm.get('nombre')?.value ;
      }

    }

## Conclusion :

Sur ce projet la premiére partie j'ai réussie à m'ensortir , mais à la fin jai bloqué sur la derniére partie.

Dépôt gitLab : https://gitlab.com/DevTheo/table-de-multiplication/-/tree/master/

