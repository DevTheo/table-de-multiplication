import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-table-multiplication',
  templateUrl: './table-multiplication.component.html',
  styleUrls: ['./table-multiplication.component.scss']
})
export class TableMultiplicationComponent implements OnInit {
  title = 'Le créateur de table :';
  table =  [1, 2, 3, 4, 5, 6, 7, 8, 9, 10] ;
  chiffre = 0 ;

  identForm!: FormGroup;

  constructor() { }

  ngOnInit(): void {
    this.identForm = new FormGroup({
      nombre:  new FormControl()
      });

  }
  // tslint:disable-next-line:typedef
  ident() {
    console.log('Données du formulaire...', this.identForm.value);
    this.chiffre = this.identForm.get('nombre')?.value ;
    }


}
