import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';

@Component({
  selector: 'app-tables-multiplication',
  templateUrl: './tables-multiplication.component.html',
  styleUrls: ['./tables-multiplication.component.scss']
})
export class TablesMultiplicationComponent implements OnInit {
  identForm!: FormGroup;
  tables =  [1, 2, 3, 4, 5, 6, 7, 8, 9, 10] ;
  demandes = [55] ;


  constructor() { }

  ngOnInit(): void {
    this.identForm = new FormGroup({
      nombre:  new FormControl()
    });

  }

  // tslint:disable-next-line:typedef
  ident() {
    console.log('Données du formulaire...', this.identForm.value);
    this.demandes = this.identForm.get('nombre')?.value ;
  }

}
